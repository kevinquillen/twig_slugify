<?php

declare(strict_types=1);

namespace Drupal\twig_slugify;

use Cocur\Slugify\Slugify;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Implements a slugify filter from the Slugify package.
 */
class SlugifyTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'twig_slugify.slugify';
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      'slugify' => new TwigFilter('slugify', [self::class, 'slugify']),
    ];
  }

  /**
   * The callback for the Twig filter.
   *
   * @param string $string
   *   The string to slugify.
   * @param array $options
   *   An array of options for Slugify.
   *
   * @return string
   *   The slugified string.
   */
  public static function slugify(string $string, array $options = []): string {
    $slugify = new Slugify();
    return $slugify->slugify($string, $options);
  }

}
